Thank you for your support request regarding [FileLink for Plik](https://gitlab.com/joendres/filelink-plik)! I'll respond as soon as I can.

Please keep in mind, that FileLink for Plik is my spare time project and I might not be able to respond instantly.

If you have additional information or further questions, please just reply to this email.

To protect your privacy your ticket %{ISSUE_ID} is not yet visible on [the project's issues list](https://gitlab.com/joendres/filelink-plik/-/boards).
